<?php  if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Display Add-ons page
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 * @license http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */
class EPKB_Add_Ons_Page {

	public function display_add_ons_page() {

		ob_start(); ?>

		<div class="wrap">
			<h1></h1>
		</div>
		<div id="ekb-admin-page-wrap" class="ekb-admin-page-wrap epkb-add-ons-container">
			<div class="epkb-config-wrapper">
				<div class="wrap" id="ekb_core_top_heading"></div>
				<div class="eckb-top-notice-message"></div>

				<div class="welcome_header">
					<div class="container-fluid">
						<div class="row">
							<div class="col-5">
								<h1><?php esc_html_e( 'Extend Your Tutorial Gate', 'tutorial-gate' ); ?></h1>
							</div>
						</div>
					</div>
				</div>          <?php

				self::display_add_ons_details();  ?>
			</div>

		</div>      <?php

		echo ob_get_clean();
	}

	/**
	 * Display all add-ons
	 */
	private static function display_add_ons_details() {

		$output = '';

		// only administrator can see licenses
		$license_content = '';
		if ( current_user_can('manage_options') ) {
			$license_content = apply_filters( 'epkb_license_fields', $output );
		}

		$tab = empty($_REQUEST['epkb-tab']) || empty($license_content) ? 'add-ons' : 'licenses';    ?>

		<div id="epkb-tabs" class="add_on_container">
			<section class="epkb-main-nav">
				<ul class="epkb-admin-pages-nav-tabs">
					<li class="nav_tab <?php echo ($tab == 'add-ons' ? 'active' : ''); ?>">
						<h2><?php _e( 'Add-ons', 'tutorial-gate' ); ?></h2>
						<p><?php _e( 'More Possibilities', 'tutorial-gate' ); ?></p>
					</li>
					<?php if ( ! empty($license_content) ) { ?>
						<li id="eckb_license_tab" class="nav_tab <?php echo ($tab == 'licenses' ? 'active' : ''); ?>">
							<h2><?php _e( 'Licenses', 'tutorial-gate' ); ?></h2>
							<p><?php _e( 'Licenses for add-ons', 'tutorial-gate' ); ?></p>
						</li>
					<?php }  ?>
				</ul>
			</section>
			<div id="add_on_panels" class="ekb-admin-pages-panel-container">
				<div class="ekb-admin-page-tab-panel container-fluid <?php echo ($tab == 'add-ons' ? 'active' : ''); ?>">
					<div class="row">   <?php

						// http://www.echoknowledgebase.com/wp-content/uploads/2017/09/product_preview_coming_soon.png

						$i18_grid = '<strong>' . __( 'Grid Layout', 'tutorial-gate' ) . '</strong>';
						$i18_sidebar = '<strong>' . __( 'Sidebar Layout', 'tutorial-gate' ) . '</strong>';
						self::add_on_product( array(
								'title' => __( 'Elegant Layouts', 'tutorial-gate' ),
								'special_note' => __( 'More ways to visualize your KB', 'tutorial-gate' ),
								'img' => 'https://www.echoknowledgebase.com/wp-content/uploads/2017/08/EL'.'AY-Featured-image.jpg',
								'desc' => sprintf( _x( 'Use %s or %s for KB Main page or combine Basic, ' .
								                       'Tabs, Grid and Sidebar layouts in many cool ways.', 'tutorial-gate' ), $i18_grid, $i18_sidebar ),
								'learn_more_url' => 'https://www.echoknowledgebase.com/wordpress-plugin/elegant-layouts/',
						) );

						$i18_list = '<strong>' . __( 'product, service or team', 'tutorial-gate' ) . '</strong>';
						self::add_on_product( array(
							'title' => __( 'Multiple Tutorial Gates', 'tutorial-gate' ),
							'special_note' => __( 'More possibilities', 'tutorial-gate' ),
							'img' => 'https://www.echoknowledgebase.com/wp-content/uploads/2017/08/MKB-Featured-image-2.jpg',
							'desc' => sprintf( _x( 'Create a new Tutorial Gate for both external and internal site as well as for each %s.',
                                                    'product, service or team', 'tutorial-gate' ), $i18_list ),
							'learn_more_url' => 'https://www.echoknowledgebase.com/wordpress-plugin/multiple-knowledge-bases/'
						) );

						self::add_on_product( array(
							'title' => __( 'Advanced Search', 'tutorial-gate' ),
							'special_note' => __( 'Enhance and analyze search', 'tutorial-gate' ),
							'img' => 'https://www.echoknowledgebase.com/wp-content/uploads/2018/09/KB-Advanced-Search-Featured-Images-1.jpg',
							'desc' => _x( 'Enhance users search experience and view search analytics including popular searches and no results searches.',
								'tutorial-gate' ),
							'learn_more_url' => 'https://www.echoknowledgebase.com/wordpress-plugin/advanced-search/'
						) );

						$i18_what = '<strong>' . __( 'Widgets and shortcodes', 'tutorial-gate' ) . '</strong>';
						self::add_on_product( array(
							'title' => __( 'Widgets', 'tutorial-gate' ),
							'special_note' => __( 'Shortcodes, Widgets, Sidebars', 'tutorial-gate' ),
							'img' => 'https://www.echoknowledgebase.com/wp-content/uploads/2015/08/Widgets-Featured-image.jpg',
							'desc' => sprintf( _x( 'Add KB Search, Most Recent Articles and other %s to your articles, sidebars and pages.',
                                                'tutorial-gate' ), $i18_what ),
							'learn_more_url' => 'https://www.echoknowledgebase.com/wordpress-plugin/widgets/'
						) );

						$i18_groups = '<strong>' . __( 'Groups', 'tutorial-gate' ) . '</strong>';
						$i18_roles = '<strong>' . __( 'KB Roles', 'tutorial-gate' ) . '</strong>';
                        self::add_on_product( array(
                            'title' => __( 'Access Manager', 'tutorial-gate' ),
                            'special_note' => __( 'Protect Your Documents', 'tutorial-gate' ),
                            'img' => 'https://www.echoknowledgebase.com/wp-content/uploads/2018/02/AM'.'GR-Featured-image.jpg',
                            'desc' => sprintf( _x( 'Restrict your Articles to certain %s using KB Categories. Assign users to specific %s within Groups.', 'tutorial-gate' ), $i18_groups, $i18_roles ),
	                        'learn_more_url' => 'https://www.echoknowledgebase.com/wordpress-plugin/access-manager/'
                        ) );

						$i18_objects = '<strong>' . __( 'PDFs, pages, posts and websites', 'tutorial-gate' ) . '</strong>';
						self::add_on_product( array(
							'title' => __( 'Links Editor for PDFs and More', 'tutorial-gate' ),
							'special_note' => __( 'In KB link to PDFs, posts and pages', 'tutorial-gate' ),
							'img' => 'https://www.echoknowledgebase.com/wp-content/uploads/2018/02/LINK-Featured-image.jpg',
							'desc' => sprintf( _x( 'Set Articles to links to %s. On KB Main Page, choose icons for your articles.', 'tutorial-gate' ), $i18_objects ),
							'learn_more_url' => 'https://www.echoknowledgebase.com/wordpress-plugin/links-editor-for-pdfs-and-more/'
						) );

						/* self::add_on_product( array(
							'title' => __( 'Article Shortcodes', 'tutorial-gate' ),
							'special_note' => __( 'Supercharge Your Articles', 'tutorial-gate' ),
							'img' => 'http://www.echoknowledgebase.com/wp-content/uploads/2017/09/product_preview_coming_soon.png',
							'desc' => __( 'Use a set of shortcodes to make it easier and faster to create professional-looking articles. ' .
							              'Additionally, your users will find these articles easier to read.'),
							'coming_when' =>  __( 'Coming in Sept', 'tutorial-gate' ),
							'#' //'learn_more_url' => 'https://www.echoknowledgebase.com/wordpress-plugin/sidebar-layout/',
						) ); */	?>

					</div>
				</div>

				<!--   LICENSES ONLY -->		<?php
				if ( ! empty($license_content) ) { ?>
					<div class="ekb-admin-page-tab-panel container-fluid <?php echo ($tab == 'licenses' ? 'active' : ''); ?>">
						<section id="ekcb-licenses" class="form_options">
							<ul>  	<!-- Add-on name / License input / status  -->   <?php
								echo $license_content;      ?>
							</ul>
						</section>
					</div>
				<?php }  ?>

			</div>
		</div>   <?php
	}

	private static function add_on_product( $values = array () ) {    ?>

		<div class="add_on_product">
			<div class="top_heading">
				<h3><?php esc_html_e($values['title']); ?></h3>
				<p><i><?php esc_html_e($values['special_note']); ?></i></p>
			</div>
			<div class="featured_img">
				<img src="<?php echo $values['img']; ?>">
			</div>
			<div class="description">
				<p>
					<?php echo wp_kses_post($values['desc']); ?>
				</p>
			</div>
			<div class="button_container">
				<?php if ( ! empty($values['coming_when']) ) { ?>
					<div class="coming_soon"><?php esc_html_e( $values['coming_when'] ); ?></div>
				<?php } else { ?>
					<a class="button primary-btn" href="<?php echo $values['learn_more_url']; ?>" target="_blank"><?php _e( 'Learn More', 'tutorial-gate' ); ?></a>
				<?php } ?>
			</div>

		</div>    <?php
	}
}

