<?php

/**
 * Setup links and information on Plugins WordPress page
 *
 * @copyright   Copyright (C) 2018, Echo Plugins
 */


/**
 * Adds various links for plugin on the Plugins page displayed on the left
 *
 * @param   array $links contains current links for this plugin
 * @return  array returns an array of links
 */
function epkb_add_plugin_action_links ( $links ) {
	$my_links = array(
			__( 'Configuration', 'tutorial-gate' )    => '<a href="' . admin_url('edit.php?post_type=' . EPKB_KB_Handler::KB_POST_TYPE_PREFIX . '1&page=epkb-kb-configuration') . '">' . esc_html__( 'Configuration', 'tutorial-gate' ) . '</a>',
			__( 'Documentation', 'tutorial-gate' )    => '<a href="https://www.echoknowledgebase.com/documentation/" target="_blank">' . esc_html__( 'Docs', 'tutorial-gate' ) . '</a>',
			__( 'Support', 'tutorial-gate' )          => '<a href="https://www.echoknowledgebase.com/contact-us/?inquiry-type=technical">' . esc_html__( 'Support', 'tutorial-gate' ) . '</a>'
	);

	return array_merge( $my_links, $links );
}
add_filter( 'plugin_action_links_' . plugin_basename(Tutorial_Gate::$plugin_file), 'epkb_add_plugin_action_links', 10, 2 );

/**
 * Add info about plugin on the Plugins page displayed on the right.
 *
 * @param $links
 * @param $file
 * @return array
 */
function epkb_add_plugin_row_meta($links, $file) {
	if ( $file != 'tutorial-gate/tutorial-gate.php' ) {
		return $links;
	}

	$links[] = '<a href="' . admin_url( 'index.php?page=epkb-welcome-page&tab=get-started' ) . '">' . esc_html__( 'Getting Started', 'tutorial-gate' ) . '</a>';
	$links[] = '<a href="https://www.echoknowledgebase.com/kb-updates-for-access-manager-search/" target="_blank">' . esc_html__( "What's New", 'tutorial-gate' ) . '</a>';

	return $links;
}
add_filter( 'plugin_row_meta', 'epkb_add_plugin_row_meta', 10, 2 );
